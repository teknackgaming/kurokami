
# Kurokami

* Game Engine: Unreal Engine 4.26
* Platform: Windows
* Single Player Action Combat Game  
* Completely Designed using Blueprints System
* 3D Environment designed using Gaea and Megascans Materials
* Can be used with Unreal Engine Versions 4.25 or newer
  
Game Developed by **[Amish Thekke Parambil](https://www.linkedin.com/in/amish-thekke-parambil/)**

